<?php get_header();?>
<section id="end">

    <div class="owl-carousel owl-theme owl-end">
        <?php
            $args = array(
                'post_type' => 'solutions',
                'posts_per_page' => -1
            );

            $q = new WP_Query($args);
        ?>

        <?php while($q->have_posts()): $q->the_post() ?>

            <div class="item">
                <div class="row">
                    <div class="col-xl-5 offset-xl-1 col-lg-7 offset-lg-1 col-sm-7 offset-sm-1 col-9 offset-1">
                        <div class="text">
                            <h3>Full lifecycle</h3>
                            <h2>"End-to-End" Solutions</h2>
                            <h4><?php the_title();?></h4>
                            <?php the_content();?>
                            <a href="#" class="btn">Contact</a>
                        </div>
                    </div>
                    <div class="col-xl-6 col-lg-4 col-sm-4 col-2 p-relative">
                        <div class="img" style="background-image: url('<?php the_post_thumbnail_url();?>');"></div>
                    </div>
                </div>
            </div>

        <?php endwhile ?>

    </div>

    <ul class="circle-float">
        <?php
            $count = 0;
        ?>
        <?php while($q->have_posts()): $q->the_post() ?>

            <li>
                <div data-id="<?php echo $count;?>" class="circle" style="background-image: url('<?php the_field('icon');?>')">
                    <div class="text">
                        <span><?php the_title();?></span>
                    </div>
                </div>
            </li>

        <?php $count = $count + 1;?>
        <?php endwhile ?>
    </ul>

</section>

<section id="contact">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h2 class="title-primary">
                    <span class="blue">
                        <div class="line"></div>
                    </span>
                    <div class="text">Contact</div>
                </h2>
                <h3><span>Get in</span> Touch <span>with Us</span></h3>

                <div class="row">

                    <div class="col-lg-8 offset-lg-2 text-center">

                        <?php echo do_shortcode('[contact-form-7 id="76" title="Contact bottom"]');?>

                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<?php get_footer();?>
