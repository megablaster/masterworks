<?php get_header();?>

<style>
    #navbar {
        top:0!important;
        position: fixed!important;
        width: 100%!important;
        z-index: 9999!important;
        margin-top: 0;
        background-color: #056277;
    }

    .bg-black {
        border-radius:0!important;
    }
</style>

    <section id="career">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-6 offset-lg-1 col-md-8 offset-md-1 p-relative">
                    <div class="text">
                        <h1>WE´RE GROWING AND ARE ALWAYS ON THE LOOKOUT FOR MOTIVATED EMPLOYEES</h1>
                        <h3>Join the Masterwork Electronics Team</h3>
                        <p>Masterwork Electronics has a unique and diverse employee culture that fosters creativity, innovation, teamwork, and communication. As a customer-driven organization, we know that each employee contributes directly to customer satisfaction and our steady corporate growth. We recognize and reward individual and team success and encourage all employees to reach new levels of achievement through personal and professional development.</p>
                        <div class="form-career">
                            <h2>I´m Interested</h2>
                            <?php echo do_shortcode('[contact-form-7 id="77" title="Career file"]');?>
                            <h4>*For consideration, upload your resume and cover letter with salary history</h4>
                        </div>
                    </div>

                </div>
                <div class="col-xl-6 offset-xl-1 col-lg-4 offset-lg-1 col-md-2 offset-md-1 p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/career/career-bg.jpg';?>')"></div>
                </div>
            </div>
        </div>

        <ul class="float-button">
            <li>
                <a href="<?php echo home_url('career');?>" class="active">
                    <span>Manufacturing engineering</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('finance');?>">
                    <span>Finance</span>
                </a>
            </li>
        </ul>

    </section>
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title-primary">
                            <span class="blue">
                                <div class="line"></div>
                            </span>
                        <div class="text">Contact</div>
                    </h2>
                    <h3><span>Get in</span> Touch <span>with Us</span></h3>

                    <div class="row">

                        <div class="col-lg-8 offset-lg-2 text-center">

                           <?php echo do_shortcode('[contact-form-7 id="76" title="Contact bottom"]');?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

<?php get_footer();?>
