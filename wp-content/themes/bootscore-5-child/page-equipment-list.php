<?php get_header('green'); ?>

<ul id="menu-about">
    <a data-menuanchor="oneSection" href="#oneSection">
        <span>INTRO</span>
    </a>
    <a data-menuanchor="twoSection" href="#twoSection">
        <span>CUSTOMER SUCCESS</span>
    </a>
    <a data-menuanchor="threeSection" href="#threeSection">
        <span>COMPLETE EQUIPMENT LIST</span>
    </a>
    <a href="#fourSection" data-menuanchor="fourSection">
        <span>MEET OUR TEAM LEADERS</span>
    </a>
    <a data-menuanchor="fiveSection" href="#fiveSection">
        <span>QUICK FACTS</span>
    </a>
    <a data-menuanchor="sixSection" href="#sixSection">
        <span>CONTACT</span>
    </a>
</ul>
<section id="fullpage">
    <section class="vertical section-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 height">
                    <div class="text">
                        <h1>State-of-the Art Electronics Manufacturing Equipment</h1>
                        <a href="#" class="btn btn-green">Contact</a>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 height p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/equipment-list.jpg';?>')
                        "></div>
                </div>
            </div>
        </div>
        <ul class="float-button">
            <li>
                <a href="<?php echo home_url('about');?>">
                    <span>About</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('why-nearshoring');?>">
                    <span>Why nearshoring?</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('production-facilities');?>">
                    <span>Production facilities</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('equipment-list');?>" class="active">
                    <span>Equipment list</span>
                </a>
            </li>
    </section>
    <section class="vertical section-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Equipment list</div>
                        </h2>
                        <h3>Time. Quality. Cost. These are the variables that drive our customers’ success.</h3>
                        <strong>That’s why we invest heavily in state-of-the-art manufacturing equipment and facilities and select only the best employees to assemble product.
                        </strong>
                        <p>With top talent using the most advanced tools in a first-rate facility, Masterwork Electronics offers unmatched quality control, impressive turnaround times, and competitive pricing.</p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 height p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/medical.jpg';?>')
                        "></div>
                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 height">

                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Equipment list</div>
                        </h2>
                        <h2 class="title" style="max-width: inherit;">Take a look to our <span>complete Equipment
                                List</span></h2>

                        <div class="col-xl-10 offset-xl-1">
                            <div class="owl-carousel owl-theme owl-equipment">
                                <div class="item">
                                    <div class="cube">
                                        <h4>Miscellaneous</h4>
                                        <ul>
                                            <li>PCA Separator, Cab Ind., Maestro 3M (2)</li>
                                            <li>Label Printer, Brady, #200M (3)</li>
                                            <li>Spray Booth, Devilbiss, #PCL-8-8 with Max Performer HVLP Gun</li>
                                            <li>IC Programmer, BP Microsystems, BP2000 with Jobmaster Software</li>
                                            <li>Cable Scan 32 (1)</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="cube">
                                        <h4>Auto insertion</h4>
                                        <ul>
                                            <li>Sequencer, Universal #2595</li>
                                            <li>Inserter, Universal #6285</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="cube">
                                        <h4>SMT</h4>
                                        <ul>
                                            <li>Screen Printer, Automatic, DEK (3)</li>
                                            <li>Screen Printer, Yamaha</li>
                                            <li>Oven, Heller 1809MK III (4)</li>
                                            <li>High Speed Mounter, Yamaha YS24 (4)</li>
                                            <li>High Speed Flexible Mounter, Yamaha YS24X (4)</li>
                                            <li>General Purpose Mounter (for NPI), Yamaha YS100</li>
                                            <li>Automatic Optical Inspector, Yamaha YSi12</li>
                                            <li>Automatic Optical Inspector, Yestech FX (5)</li>
                                            <li>Automatic Optical Inspector, Yestech F1 (2)</li>
                                            <li>Automatic Optical Inspector, Yestech 2050</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="cube">
                                        <h4>BGA/FINE PITCH PLACEMENT/REWORK STATION</h4>
                                        <ul>
                                            <li>BGA/CSP/Fine Pitch Rework, Metcal BGA 3591 (2)</li>
                                            <li>X-ray, 2 Dimensional, Glenbrook, RTX Dual Vu (2ç)</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="cube">
                                        <h4>Wave/Wash</h4>
                                        <ul>
                                            <li>Wave Solder, Electrovert Gold</li>
                                            <li>Wash, Westek, Triton IV SMT</li>
                                            <li>Wash, Electrovert Aquastorm 100</li>
                                            <li>Wash, Electrovert Vectra ES (2)</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="cube">
                                        <h4>Test</h4>
                                        <ul>
                                            <li>In Circuit Tester (ICT), Hewlett Packard, HP3070 Series 3</li>
                                            <li>Manufacturing Defect Analyzer (MDA), Checksum, Model 8</li>
                                            <li>Oscilloscope, HP54501A</li>
                                            <li>Oscilloscope, Tektronix, #2205 (2)</li>
                                            <li>Oscilloscope, Tektronix, TDS320 (4)</li>
                                            <li>Oscilloscope, Tektronix, TDS380 (1)</li>
                                            <li>Oscilloscope, Tektronix, TDS420 (1)</li>
                                            <li>Function Generator, Leader, #120B (2)</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="cube">
                                        <h4>Inspection</h4>
                                        <ul>
                                            <li>Inspection Microscope, Vision Engineering, Mantis (5)</li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="item">
                                    <div class="cube">
                                        <h4>Hand soldering</h4>
                                        <ul>
                                            <li>Soldering Station, Hakko 9XX Series (25)</li>
                                            <li>Soldering Station, Metcal (8), including DSI Unit</li>
                                            <li>Rework Station, Hakko, #471 (4)</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Our team</div>
                        </h2>
                        <h2 class="title">Get to know the leaders of <span>our team</span></h2>
                        <div class="row">
                            <?php
                            $args = array(
                                'post_type' => 'ourteam',
                                'posts_per_page' => -1
                            );

                            $q = new WP_Query($args);
                            ?>

                            <?php while($q->have_posts()): $q->the_post() ?>
                                <div class="col-xl-3 col-md-3">
                                    <div class="circle" style="background-image: url('<?php the_post_thumbnail_url();
                                    ?>')">
                                        <a href="#<?php echo the_ID();?>" class="text fancybox">Know more</a>
                                    </div>
                                    <h3><?php the_title();?></h3>
                                </div>

                                <div id="<?php echo the_ID();?>" style="display: none;max-width: 700px;width:100%;
                            background-color:#606060;">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <h2 style="color:white;"><?php the_title();?></h2>

                                                <?php if(have_rows('videos')): ?>

                                                    <?php while(have_rows('videos')): the_row() ?>
                                                        <a class="fancybox" href="<?php the_sub_field('url_video');?>"
                                                           style="display:block;font-weight:300;font-size:14px;
                                                       color:white;">
                                                            <?php the_sub_field('title');?>
                                                        </a>
                                                    <?php endwhile ?>

                                                <?php endif ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php wp_reset_postdata();?>
    </section>
    <section class="vertical section-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Quick facts</div>
                        </h2>
                        <h2 class="title">Get to know some facts about <span>Masterwork Electronics</span></h2>
                        <div class="row">
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-1.png';?>')">
                                </div>
                                <h3>HQ: San Diego Revenue: ~ $100M / 550+ H/C</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-2.png';?>')">
                                </div>
                                <h3>Founded in 1994. 26 years of successful operations, 16 years in Mexicali</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-3.png';?>')">
                                </div>
                                <h3>Acquired in June 2018 by Hidden Harbor Capital Partners (hh-cp.com)</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-4.png';?>')">
                                </div>
                                <h3><span>Sector Focus:</span>
                                    <ul>
                                        <li>Industrial · High-End Communications</li>
                                        <li>IOT · Power/Energy</li>
                                        <li>Transportation/Automotive</li>
                                        <li>Medical · A&D</li>
                                    </ul>
                                </h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-5.png';?>')">
                                </div>
                                <h3>Mexicali Operations
                                    original PCBA Site opened in 2004.
                                    Two more Sites opened in 2018 (Box)
                                    and 2019 (Fulfillment) ~ 90K+ SF Capacity</h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-6">
        <div class="top" id="contact">
            <div class="row">
                <div class="col-xl-10 offset-xl-1">
                    <h2 class="title-primary">
                            <span class="blue">
                                <div class="line"></div>
                            </span>
                        <div class="text">Contact</div>
                    </h2>
                    <h3><span>Get in</span> Touch <span>with Us</span></h3>
                </div>
                <div class="col-lg-6 offset-lg-3 text-center">

                    <div class="form">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Subject">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" cols="5" rows="5" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="#" class="btn btn-submit">Submit now</a>

                </div>
            </div>
        </div>
    </section>
</section>

<?php get_footer(); ?>

