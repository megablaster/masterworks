<?php get_header();?>
<section id="case-study">
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-3 offset-xl-2">
                <div class="text">
                    <h2>Welcome</h2>
                    <h1>GET TO KNOW THE SOLUTIONS THAT FOREVER CHANGED OUR CLIENTS FUTURE</h1>
                    <p>Welcome to the Masterwork Electronics Case Study Blog, here you´ll find stories of how we helped our clients anc collaborators to find creative and innovative solutions to their problems.</p>
                    <div class="form-career">
                        <h2>I´m Interested</h2>
                        <?php echo do_shortcode('[contact-form-7 id="79" title="Case study file"]');?>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 offset-xl-2 p-relative">
                <div class="scroll">
                    <h3>Top Stories</h3>
                    <?php
                        $args = array(
                            'post_type' => 'posts',
                            'posts_per_page' => -1
                        );

                        $q = new WP_Query($args);
                    ?>

                    <?php while($q->have_posts()): $q->the_post() ?>
                        <div class="row">
                            <div class="col-xl-4"></div>
                            <div class="col-xl-8">
                                <div class="date">Date</div>
                                <div class="author">Author</div>
                                <h3><?php the_title(); ?></h3>
                                <?php the_content();?>
                            </div>
                        </div>
                    <?php endwhile ?>

                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer();?>