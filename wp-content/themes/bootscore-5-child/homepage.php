<?php
/*
* Template name: homepage
*/
get_header();
?>
<?php echo do_shortcode('[rev_slider alias="home"][/rev_slider]');?>
<section id="homepage">

    <section id="about">
        <div class="container">
            <div class="row">
                <div class="col-xl-7">

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">About</div>
                    </h2>

                    <h2>We are the talent <span>behind some of the worlds</span> more innovative products.</h2>
                    <p>Masterwork Electronics is today one of the premier Electronics Manufacturing Service (EMS) providers With over 25 years of experience.</p>
                    <p>We serve a diverse mix of Original Equipment Manufacturers (OEMs) from North America and beyond. Every day, Masterwork Electronics products produces more than 10,000 printed circuit board assemblies (PCBA) for leading global copanies.</p>
                    <p>Or boards are the "brains" behind some of the world´s most innovative products, like batery chargers for electric vehicles, the latest Coke and Pepsi "smart" machines and circuit boards for cutting-edge M2M devices.</p>

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">What we do</div>
                    </h2>

                    <h2>We found <span>a solution to your problem</span> On Record Time</h2>
                    <p>Our Electronics Manufacturing Services (EMS) span many industries, from consumer electronics and telecommunications to vending and industrial eelctronics manufacturing. Our quality control and feedback systems guarantee our electronics contract manufacturing services are the best in the industry, delivered on-time with the highest quality.</p>

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">Quick facts</div>
                    </h2>

                    <ul>
                        <li>HQ: San Diego / Revenue: ~ $100M / 550+ H/C</li>
                        <li>Founded in 1994. 26 years of successful operations, 16 years in Mexicali</li>
                        <li>Acquired in June 2018 by Hidden Harbor Capital Partners (hh-cp.com)</li>
                        <li>Mexicali Operations original PCBA Site opened in 2004. Two more Sites opened in 2018
                            (Box) and 2019 (Fulfillment) ~ 90K+ SF Capacity</li>
                        <li>Sector Focus: Industrial; High-End Communications; IOT; Power/Energy; Transportation/Automotive; Medical; A&D</li>
                    </ul>

                </div>

                <div class="col-xl-4 offset-xl-1">

                    <div class="img" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/bg-about.jpg';?>');">
                        <div class="text">
                            <h2>25+</h2>
                            <h4>Years of <span>experience</span></h4>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>

    <section id="value">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">Our values</div>
                    </h2>

                    <h2 class="title">Get Absolute <span>Professional Experience</span> From Our Company</h2>

                    <div class="row">
                        <div class="col-xl-4">
                            <div class="cube">
                                <h3>Continuous Improvement</h3>
                                <p>We passionately live in a Kaizen “state of mind”, never satisfied with our latest accomplishments, and always looking for things we can correct and ways to improve.</p>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="cube">
                                <h3>Respect for People</h3>
                                <p>We foster and promote an environment of humanity and respect by treating each other as equals, respecting each other's opinions and suggestions, and encourage a willingness to accept and adapt to change.</p>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="cube">
                                <h3>Integrity</h3>
                                <p>We strive to maintain the highest integrity through honesty and respect for our partners, our people, and our processes. We value ethics and transparency in ourselves and our people.</p>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="cube">
                                <h3>Teamwork</h3>
                                <p>Success is achieved by striving toward our common shared goals and objectives as a team. We value and are open to differing opinions.</p>
                            </div>
                        </div>
                        <div class="col-xl-4">
                            <div class="cube">
                                <h3>Customer Focus</h3>
                                <p>Listening and understanding to our customer's needs drive us in our daily activities. We are committed to exceed their expectations with high quality and on time delivery, while remaining flexible and responsive to schedule changes. Total Customer Satisfaction is our goal.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="certification">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">Cerficications</div>
                    </h2>

                    <h2 class="title"><span>Masterwork’s</span> Quality excellence <span>Is our people, processes and tools.</span></h2>
                    Our relentless commitment to quality that has built Masterwork’s reputation for quality excellence is demonstrated in our people, processes and tools. We are ISO 9001:2105 certified and C-TPAT compliant and continue to identify opportunities to improve our performance.
                    <ul>
                        <li>- State of the art SMT equipment with in-line Solder Paste Inspection and Automated Optical Inspection.</li>
                        <li>- Customer focused production lines and Product-specific Work Instructions ensure focus on each customer’s unique requirements.</li>
                        <li>- Lean principles, 5S practices and process performance metrics drive our daily activities.</li>
                        <li>- Factory-wide ESD controls, Automated Material Handling systems and recently upgraded ERP system all contribute to our Culture of Quality</li>
                    </ul>

                    <div class="owl-carousel owl-certification owl-theme">

                        <div class="item">
                            <div class="logo" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-1.jpg';?>');">
                                <div class="text">
                                    <div class="center">
                                        <h3>ISO 9001 System Certified</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="logo" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-2.jpg';?>');">
                                <div class="text">
                                    <div class="center">
                                        <h3>ISO 9001 System Certified</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="logo" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-3.jpg';?>');">
                                <div class="text">
                                    <div class="center">
                                        <h3>ISO 9001 System Certified</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="item">
                            <div class="logo" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-3.jpg';?>');">
                                <div class="text">
                                    <div class="center">
                                        <h3>ISO 9001 System Certified</h3>
                                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis ipsum suspendisse ultrices gravida. Risus commodo viverra maecenas accumsan lacus vel facilisis.</p>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/line-certification.jpg';?>"  class="img-fluid">

                </div>
            </div>
        </div>
    </section>

    <section id="clients">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">Our clients</div>
                    </h2>

                    <h2 class="title">Our Clients <span>Testimonials.</span></h2>

                    <div class="row">
                        <div class="col-xl-6">
                            <div class="person">
                                <img src="<?php echo get_stylesheet_directory_uri().'/img/home/person-1.jpg';?>" class="img-fluid">
                                <h3>David Johnson</h3>
                                <h4>ceo</h4>
                                <p>"Masterwork Electronics help me make my product came to live"</p>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="person">
                                <img src="<?php echo get_stylesheet_directory_uri().'/img/home/person-2.jpg';?>" class="img-fluid">
                                <h3>Jason Meyers</h3>
                                <h4>Motorola</h4>
                                <p>"Masterwork Electronics provided a complete solution from engineering to logistics"</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="near">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">Nearshoring</div>
                    </h2>

                    <h2 class="title">Manufacturing <span>and distribution</span> from Mexicali.</h2>
                    <p class="subtitle">Delivering the highest quality product in the shortest amount of time at the most competitive price has been the challenge manufacturers have faced for decades. With lower operating costs in Mexico coupled with the benefits of being closer to the ultimate customers, manufacturing and distribution from Mexicali provides a competitive advantage.</p>

                    <div class="row">

                        <div class="col-xl-3">
                            <img src="<?php echo get_stylesheet_directory_uri().'/img/home/near-1.jpg';?>" class="img-fluid">
                            <h3>Quality</h3>
                            <p>Easy access to the factory and your products provides additional security and comfort in a complex and competitive environment.</p>
                        </div>

                        <div class="col-xl-3">
                            <img src="<?php echo get_stylesheet_directory_uri().'/img/home/near-2.jpg';?>" class="img-fluid">
                            <h3>Time</h3>
                            <p>Time to market, Product Transit time and Transport costs are reduced by the proximity of our factory.</p>
                        </div>

                        <div class="col-xl-3">
                            <img src="<?php echo get_stylesheet_directory_uri().'/img/home/near-3.jpg';?>" class="img-fluid">
                            <h3>Lower costs</h3>
                            <p>Your Products’ total cost reduces with shorter lead times; less inventory on your books; lower freight costs and affordable labor.</p>
                        </div>

                        <div class="col-xl-3">
                            <img src="<?php echo get_stylesheet_directory_uri().'/img/home/near-4.jpg';?>" class="img-fluid">
                            <h3>Trust</h3>
                            <p>The ability to operate with US-based transactions and enhanced face-to-face interaction build a trustworthy Supply Chain.</p>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </section>

    <section id="production">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">Production facilities</div>
                    </h2>

                    <h2 class="title"><span>Masterwork</span> offers a safe and convenient <span>low cost region manufacturing solution.</span></h2>
                    <p><strong>With over 90,000 square feet of manufacturing facilities</strong> at our Calafia Industrial Park campus in Mexicali, only minutes away from the Calexico East border crossing, Masterwork offers a safe and convenient low-cost region manufacturing solution for our North American customers. </p>
                    <p>Our recently expanded facilities present ample space for our centers of excellence for <strong>PCBA, Cable and Harness assemblies, and Box-build production as well as offering space for our future growth.</strong></p>
                    <p><strong>Masterwork excels at high mix, low volume PCB Assembly with industry leading change-over times, three operating shifts and common equipment configurations.</strong></p>
                    <p>Our equipment can be configured to cater to higher volumes and have demonstrated the costs needed to compete effectively at both the high and low volumes.</p>
                </div>
            </div>
        </div>
        <img src="<?php echo get_stylesheet_directory_uri().'/img/home/production-img.jpg';?>" class="img-fluid">
    </section>

    <section id="capabilities">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">Capabilities</div>
                    </h2>

                    <h2 class="title">
                        <span>Extensive Range of</span> Electronics Manufacturing Services <span>(EMS).</span>
                    </h2>

                    <h3>Custominez Printed Circuit Board Assembly (PCBA)</h3>
                    <p>Masterwork Electronics provides printed circuit board assembly services to a variety of different industries. We have the capacity to handle jobs of any size and complexity and our efficient manufacturing process and nearshore location supports rapid turnaround times.</p>
                    <h3>Cable Harness Assembly</h3>
                    <p>We have the capability and expertise to work with a wide variety of cables & harnesses, including:</p>
                    <ul>
                        <li>• Flat ribbon</li>
                        <li>• Coax cable assemblies</li>
                        <li>• Discrete harnesses</li>
                        <li>• Multi conductor</li>
                        <li>• Sensor cable/harnesses</li>
                    </ul>
                    <h3>Box Build</h3>
                    <p>We customize Box-build production lines for just-in-time order fulfillment and if preferred by the customer, build to stock and provide on-demand fulfillment directly to our customers’ end users. We have extensive warehouse operations to handle large components and inventory items as well as to carry pallets of finished goods for our customers.</p>
                    <h3>Direct Order Fulfillment and Repair Services</h3>
                    <p>Our operations are tailored to provide post-production requirements for Warehousing, Pick-and-Pack and Direct Order Fulfillment for shipment to your end customers. We also handle Accessories and any warranty repair and other post sale services you may need.</p>
                </div>
            </div>
        </div>
    </section>

    <section id="career">
        <div class="container">
            <div class="row">
                <div class="col-xl-12">

                    <h2 class="title-primary">
					   <span class="blue">
						  <div class="line"></div>
					   </span>
                        <div class="text">Careers</div>
                    </h2>

                    <h2 class="title">
                        <span>We’re growing</span> and are always <span>on the lookout for motivated employees.</span>
                    </h2>

                    <p>Take a look at the job opportunities we have for you:</p>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-4 offset-xl-1">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/career-1.jpg';?>" class="img-fluid">
                    <h4>Manufacturing Engineering</h4>
                </div>
                <div class="col-xl-4 offset-xl-1">
                    <img src="<?php echo get_stylesheet_directory_uri().'/img/home/career-2.jpg';?>" class="img-fluid">
                    <h4>Finance</h4>
                </div>
            </div>
        </div>
    </section>

    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <h2 class="title-primary">
                        <span class="blue">
                            <div class="line"></div>
                        </span>
                        <div class="text">Contact</div>
                    </h2>
                    <h3><span>Get in</span> Touch <span>with Us</span></h3>

                    <div class="row">

                        <div class="col-lg-8 offset-lg-2 text-center">

                            <?php echo do_shortcode('[contact-form-7 id="76" title="Contact bottom"]');?>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>

</section>
<?php get_footer(); ?>