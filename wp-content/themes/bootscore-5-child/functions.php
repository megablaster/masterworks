<?php

// style and scripts
add_action( 'wp_enqueue_scripts', 'bootscore_5_child_enqueue_styles' );
function bootscore_5_child_enqueue_styles() {
     
	// style.css
	wp_enqueue_style( 'owl-css', get_stylesheet_directory_uri() . '/script/owl/assets/owl.carousel.min.css' ); 
 	wp_enqueue_style( 'owl-theme-css', get_stylesheet_directory_uri() . '/script/owl/assets/owl.theme.default.min.css' ); 
 	wp_enqueue_style( 'fancybox-css', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.css' ); 
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' ); 
	 
	// custom.js
    if (is_page('about') || is_page('why-nearshoring') || is_page('production-facilities') || is_page('equipment-list')){
        wp_enqueue_script('pagefull-js', 'https://cdnjs.cloudflare.com/ajax/libs/fullPage.js/2.6.6/jquery.fullPage.min.js',
            false, '', true);
        wp_enqueue_script('full-js', get_stylesheet_directory_uri().'/js/fullpage.js',
            false, '', true);
    }
	wp_enqueue_script('owl-js', get_stylesheet_directory_uri() . '/script/owl/owl.carousel.min.js', false, '', true);
     wp_enqueue_script('fancy-js', 'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js', false, '', true);
	wp_enqueue_script('custom-js', get_stylesheet_directory_uri() . '/js/custom.js', false, '', true);

}

//Widgets
function dcms_agregar_nueva_zona_widgets() {

	register_sidebar( array(
		'name'          => 'Idioma',
		'id'            => 'lang-top',
		'description'   => 'Posición superior para módulo de idioma',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	
}

add_action( 'widgets_init', 'dcms_agregar_nueva_zona_widgets' );


add_filter( 'wpcf7_autop_or_not', '__return_false' );


