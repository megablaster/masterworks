<?php get_header();?>
<style>
	@media only screen and (max-width : 992px) {
		#navbar {
			background-color:#056277;
			margin-top: inherit;
			position: relative;
		}
	}
</style>
<section id="contact-page">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xl-6 order-xl-1 order-lg-2">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d13438.302045329921!2d-115.40517751556831!3d32.644125999832305!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80d7717482723747%3A0x70cf491ec5b1178b!2sMasterwork%20Electronics%20de%20Mexico%2C%20S.A.%20de%20C.V.!5e0!3m2!1ses-419!2smx!4v1624515669018!5m2!1ses-419!2smx" width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy"></iframe>
			</div>
			<div class="col-xl-5 order-xl-2 order-lg-1">
				<div class="margin-left">

					<h2 class="title-primary">
						<span class="blue">
							<div class="line"></div>
						</span>
						<div class="text">Lets talk</div>
					</h2>

					<h3><span>Get in</span> Touch <span>with Us</span></h3>

					<div class="row">

						<div class="col-lg-12 text-center">

							<div class="form">
								<div class="row">
									<div class="col-lg-6">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Your Name">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Email">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Phone">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="City">
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<select class="form-control"></select>
										</div>
									</div>
									<div class="col-lg-6">
										<div class="form-group">
											<select class="form-control"></select>
										</div>
									</div>
									<div class="col-lg-12">
										<div class="form-group">
											<input type="text" class="form-control" placeholder="Subject">
										</div>
										<div class="form-group">
											<textarea class="form-control" cols="5" rows="5" placeholder="Message"></textarea>
										</div>
									</div>
								</div>
							</div>

							<a href="#" class="btn btn-submit">Submit now</a>

						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<?php get_footer();?>