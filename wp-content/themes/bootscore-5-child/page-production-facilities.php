<?php get_header('green'); ?>

<ul id="menu-about">
    <a data-menuanchor="oneSection" href="#oneSection">
        <span>INTRO</span>
    </a>
    <a data-menuanchor="twoSection" href="#twoSection">
        <span>GEOGRAPHICS</span>
    </a>
    <a href="#threeSection" data-menuanchor="threeSection">
        <span>MEET OUR TEAM LEADERS</span>
    </a>
    <a data-menuanchor="fourSection" href="#fourSection">
        <span>QUICK FACTS</span>
    </a>
    <a data-menuanchor="fiveSection" href="#fiveSection">
        <span>CONTACT</span>
    </a>
</ul>
<section id="fullpage">
    <section class="vertical section-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 height">
                    <div class="text">
                        <h1>Advanced Electronics Manufacturing Production Facilities</h1>
                        <p>Our customers' products are shipped by all the major carriers.</p>
                        <a href="#" class="btn btn-green">Contact</a>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 height p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/production-facilities.jpg';?>')
                        "></div>
                </div>
            </div>
        </div>
        <ul class="float-button">
            <li>
                <a href="<?php echo home_url('about');?>">
                    <span>About</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('why-nearshoring');?>">
                    <span>Why nearshoring?</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('production-facilities');?>" class="active">
                    <span>Production facilities</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('equipment-list');?>">
                    <span>Equipment list</span>
                </a>
            </li>
        </ul>
    </section>
    <section class="vertical section-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Production facilities</div>
                        </h2>
                        <h3>Masterwork's 40,000 sq. ft. facility is staffed by a well-experienced workforce of engineers, assemblers, and support personnel.</h3>
                        <strong>They’re on the same time zone and share the same customer-driven culture as our California team. We’ve invested heavily in the most advanced Electronics Manufacturing Services equipment and rigorously train and re-train employees to ensure adherence to Masterwork's stringent quality control standards. </strong>
                        <p>Mexicali is just over the border from California.</p>
                        <p>The capital of the state of Baja California, Mexicali is a booming industrial hub, home to top U.S. companies in the electronics, aerospace, automotive, and food processing sectors. Newly designed highways make transportation and delivery more convenient, less costly, and accessible to ports, rail systems, and border crossings.</p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 height p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/map.jpg';?>')
                        "></div>
                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Our team</div>
                        </h2>
                        <h2 class="title">Get to know the leaders of <span>our team</span></h2>
                        <div class="row">
                            <?php
                            $args = array(
                                'post_type' => 'ourteam',
                                'posts_per_page' => -1
                            );

                            $q = new WP_Query($args);
                            ?>

                            <?php while($q->have_posts()): $q->the_post() ?>
                                <div class="col-xl-3 col-md-3">
                                    <div class="circle" style="background-image: url('<?php the_post_thumbnail_url();
                                    ?>')">
                                        <a href="#<?php echo the_ID();?>" class="text fancybox">Know more</a>
                                    </div>
                                    <h3><?php the_title();?></h3>
                                </div>

                                <div id="<?php echo the_ID();?>" style="display: none;max-width: 700px;width:100%;
                            background-color:#606060;">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <h2 style="color:white;"><?php the_title();?></h2>

                                                <?php if(have_rows('videos')): ?>

                                                    <?php while(have_rows('videos')): the_row() ?>
                                                        <a class="fancybox" href="<?php the_sub_field('url_video');?>"
                                                           style="display:block;font-weight:300;font-size:14px;
                                                       color:white;">
                                                            <?php the_sub_field('title');?>
                                                        </a>
                                                    <?php endwhile ?>

                                                <?php endif ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php wp_reset_postdata();?>
    </section>
    <section class="vertical section-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Quick facts</div>
                        </h2>
                        <h2 class="title">Get to know some facts about <span>Masterwork Electronics</span></h2>
                        <div class="row">
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-1.png';?>')">
                                </div>
                                <h3>HQ: San Diego Revenue: ~ $100M / 550+ H/C</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-2.png';?>')">
                                </div>
                                <h3>Founded in 1994. 26 years of successful operations, 16 years in Mexicali</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-3.png';?>')">
                                </div>
                                <h3>Acquired in June 2018 by Hidden Harbor Capital Partners (hh-cp.com)</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-4.png';?>')">
                                </div>
                                <h3><span>Sector Focus:</span>
                                    <ul>
                                        <li>Industrial · High-End Communications</li>
                                        <li>IOT · Power/Energy</li>
                                        <li>Transportation/Automotive</li>
                                        <li>Medical · A&D</li>
                                    </ul>
                                </h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-5.png';?>')">
                                </div>
                                <h3>Mexicali Operations
                                    original PCBA Site opened in 2004.
                                    Two more Sites opened in 2018 (Box)
                                    and 2019 (Fulfillment) ~ 90K+ SF Capacity</h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-6">
        <div class="top" id="contact">
            <div class="row">
                <div class="col-xl-8 offset-xl-1">
                    <h2 class="title-primary">
                            <span class="blue">
                                <div class="line"></div>
                            </span>
                        <div class="text">Contact</div>
                    </h2>
                    <h3><span>Get in</span> Touch <span>with Us</span></h3>
                </div>
                <div class="col-lg-6 offset-lg-3 text-center">

                    <div class="form">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Subject">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" cols="5" rows="5" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="#" class="btn btn-submit">Submit now</a>

                </div>
            </div>
        </div>
    </section>
</section>

<?php get_footer(); ?>

