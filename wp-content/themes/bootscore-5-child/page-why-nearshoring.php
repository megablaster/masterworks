<?php get_header('green'); ?>

<ul id="menu-about">
    <a data-menuanchor="oneSection" href="#oneSection">
        <span>INTRO</span>
    </a>
    <a data-menuanchor="twoSection" href="#twoSection">
        <span>FACILITIES</span>
    </a>
    <a data-menuanchor="threeSection" href="#threeSection">
        <span>KEY BENEFITS</span>
    </a>
    <a href="#fourSection" data-menuanchor="fourSection">
        <span>MEET OUR TEAM LEADERS</span>
    </a>
    <a data-menuanchor="fiveSection" href="#fiveSection">
        <span>QUICK FACTS</span>
    </a>
    <a data-menuanchor="sixSection" href="#sixSection">
        <span>CONTACT</span>
    </a>
</ul>
<section id="fullpage">
    <section class="vertical section-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 height">
                    <div class="text">
                        <h1>a Leader in electronic manufacturing services</h1>
                        <p>For all manufacturers, the eternal challenge is to produce the highest quality product in the shortest amount of time at the most competitive price. For decades, companies sought to meet that challenge by establishing operations on distant continents.</p>
                        <a href="#" class="btn btn-green">Contact</a>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 height p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/section-2-2.jpg';?>')
                            "></div>
                </div>
            </div>
        </div>
        <ul class="float-button">
            <li>
                <a href="<?php echo home_url('about');?>">
                    <span>About</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('why-nearshoring');?>" class="active">
                    <span>Why nearshoring?</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('production-facilities');?>">
                    <span>Production facilities</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('equipment-list');?>">
                    <span>Equipment list</span>
                </a>
            </li>
        </ul>
    </section>
    <section class="vertical section-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Production facilities</div>
                        </h2>
                        <h3>Locating production facilities closer to home, or nearshoring, is the smart choice for U.S. manufacturers</h3>
                        <strong>Persistent problems stemming from long distances and poor communication, cultural barriers and delivery obstacles have reinforced the value of what Masterwork Electronics has known for years—locating production facilities closer to home, or nearshoring, is the smart choice for U.S. manufacturers.
                        </strong>
                        <p>In an industry as specialized and customized as electronic component, cable and harness,
                            and print-ed circuit board assemblies, this provides a huge value to our customers.</p>
                        <p>In our case, Mexicali, Mexico is the ideal location for Masterwork's high-quality/lower
                        cost/faster delivery operations model.</p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 height p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/section-2-truck.jpg';?>')
                            "></div>
                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Key benefits</div>
                        </h2>
                        <h2 class="title">What are the <span>key benefits of Nearshoring?</span></h2>
                        <div class="row">

                            <div class="col-xl-8 offset-xl-2">
                                <div class="owl-carousel owl-theme owl-benefits">
                                    <div class="item">
                                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/money.jpg';?>')"></div>
                                        <div class="text">
                                            <h3>Money</h3>
                                            <p>Shorter lead times mean less inventory tied up.</p>
                                            <p>Affordable labor costs in Mexico make us competitive.</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="img" style="background-image: url('<?php echo
                                            get_stylesheet_directory_uri().'/img/about/trust.jpg';?>')"></div>
                                        <div class="text">
                                            <h3>Trust</h3>
                                            <p>Build face-to-face, personal relationships with customers and suppliers.</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="img" style="background-image: url('<?php echo
                                            get_stylesheet_directory_uri().'/img/about/quality.jpg';?>')"></div>
                                        <div class="text">
                                            <h3>Quality</h3>
                                            <p>No leap of faith required when you can visit your product or your partners.</p>
                                        </div>
                                    </div>
                                    <div class="item">
                                        <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/time.jpg';?>')"></div>
                                        <div class="text">
                                            <h3>Time</h3>
                                            <p>Transit time and transport costs decrease due to the proximity of the manufacturing plant. Costly International air freight or lengthy ocean freight no longer applies.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Our team</div>
                        </h2>
                        <h2 class="title">Get to know the leaders of <span>our team</span></h2>
                        <div class="row">
                            <?php
                            $args = array(
                                'post_type' => 'ourteam',
                                'posts_per_page' => -1
                            );

                            $q = new WP_Query($args);
                            ?>

                            <?php while($q->have_posts()): $q->the_post() ?>
                                <div class="col-xl-3 col-md-3">
                                    <div class="circle" style="background-image: url('<?php the_post_thumbnail_url();
                                    ?>')">
                                        <a href="#<?php echo the_ID();?>" class="text fancybox">Know more</a>
                                    </div>
                                    <h3><?php the_title();?></h3>
                                </div>

                                <div id="<?php echo the_ID();?>" style="display: none;max-width: 700px;width:100%;
                            background-color:#606060;">
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xl-12">
                                                <h2 style="color:white;"><?php the_title();?></h2>

                                                <?php if(have_rows('videos')): ?>

                                                    <?php while(have_rows('videos')): the_row() ?>
                                                        <a class="fancybox" href="<?php the_sub_field('url_video');?>"
                                                           style="display:block;font-weight:300;font-size:14px;
                                                       color:white;">
                                                            <?php the_sub_field('title');?>
                                                        </a>
                                                    <?php endwhile ?>

                                                <?php endif ?>

                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php endwhile ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php wp_reset_postdata();?>
    </section>
    <section class="vertical section-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Quick facts</div>
                        </h2>
                        <h2 class="title">Get to know some facts about <span>Masterwork Electronics</span></h2>
                        <div class="row">
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-1.png';?>')">
                                </div>
                                <h3>HQ: San Diego Revenue: ~ $100M / 550+ H/C</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-2.png';?>')">
                                </div>
                                <h3>Founded in 1994. 26 years of successful operations, 16 years in Mexicali</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-3.png';?>')">
                                </div>
                                <h3>Acquired in June 2018 by Hidden Harbor Capital Partners (hh-cp.com)</h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-4.png';?>')">
                                </div>
                                <h3><span>Sector Focus:</span>
                                    <ul>
                                        <li>Industrial · High-End Communications</li>
                                        <li>IOT · Power/Energy</li>
                                        <li>Transportation/Automotive</li>
                                        <li>Medical · A&D</li>
                                    </ul>
                                </h3>
                            </div>
                            <div class="col">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-5.png';?>')">
                                </div>
                                <h3>Mexicali Operations
                                    original PCBA Site opened in 2004.
                                    Two more Sites opened in 2018 (Box)
                                    and 2019 (Fulfillment) ~ 90K+ SF Capacity</h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-6">
        <div class="top" id="contact">
            <div class="row">
                <div class="col-xl-8 offset-xl-1">
                    <h2 class="title-primary">
                            <span class="blue">
                                <div class="line"></div>
                            </span>
                        <div class="text">Contact</div>
                    </h2>
                    <h3><span>Get in</span> Touch <span>with Us</span></h3>
                </div>
                <div class="col-lg-6 offset-lg-3 text-center">

                    <div class="form">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Email">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Subject">
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" cols="5" rows="5" placeholder="Message"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>

                    <a href="#" class="btn btn-submit">Submit now</a>

                </div>
            </div>
        </div>
    </section>
</section>

<?php get_footer(); ?>
