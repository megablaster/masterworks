jQuery(document).ready(function($){
    $('#fullpage').fullpage({
        sectionsColor: ['#dedede', '#ffffff', '#ffffff', '#ffffff', '#ffffff','#ffffff'],
        sectionSelector: '.vertical',
        slideSelector: '.horizontal',
        navigation: true,
        slidesNavigation: true,
        controlArrows: false,
        anchors: ['oneSection', 'twoSection', 'threeSection', 'fourSection', 'fiveSection','sixSection'],
        menu: '#menu-about',
    });
});