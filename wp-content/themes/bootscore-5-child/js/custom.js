jQuery(document).ready(function ($) {

	$('.section-5 .img').hover(function(){
		$('.section-5 h3').css('opacity','0');
		$(this).next().css('opacity','1');
	});

	$('.circle-float .circle').on('click',function(){
		var id = $(this).data('id');
		end.trigger('to.owl.carousel', id);
	});

	var end = $('.owl-end');
	end.owlCarousel({
		loop:true,
		nav:false,
		margin:0,
		dots:false,
		responsive:{
			0:{
				items:1,
				dots:true
			},
			600:{
				items:1,
				dots:true
			},
			768: {
				items:1,
				dots:true
			},
			992:{
				items:1,
				dots:true
			},
			1366: {
				items:1
			}
		}
	});

	$('.owl-certification').owlCarousel({
	    loop:true,
	    nav:false,
	    margin:40,
	    navText : ["<i class='fa fa-chevron-left fa-3x'></i>","<i class='fa fa-chevron-right fa-3x'></i>"],
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        768: {
	        	items:2
	        },
	        1000:{
	            items:3
	        },
	        1366: {
	        	items:3
	        }
	    }
	});

	$('.owl-equipment').owlCarousel({
		nav:true,
		margin:13,
		loop:true,
		navText: ['<span class="fas fa-chevron-left fa-2x"></span>','<span class="fas fa-chevron-right fa-2x"></span>'],
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2,
				dots:true
			},
			768: {
				items:2,
				dots:true
			},
			1000:{
				items:3,
				dots:false
			},
			1366: {
				items:3,
				dots:false
			}
		}
	});

	$('.owl-industrial').owlCarousel({
	    loop:false,
	    nav:false,
	    margin:0,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:2
	        },
	        768: {
	        	items:3
	        },
	        1000:{
	            items:4
	        },
	        1366: {
	        	items:6
	        }
	    }
	});

	$('.owl-benefits').owlCarousel({
		loop:true,
		nav:false,
		margin:100,
		center:true,
		responsive:{
			0:{
				items:1
			},
			600:{
				items:2
			},
			768: {
				items:2
			},
			1000:{
				items:3
			},
			1366: {
				items:3
			}
		}
	});

	$(".fancybox").fancybox({
		helpers: {
			overlay: {
				locked: false
			}
		}
	});

	$('.burger').on('click', function(){
		$('.menu-mobile').toggleClass('view');
		$('#grey').toggleClass('view');
	});

	$('.close-icon').on('click', function(){
		$('.menu-mobile').toggleClass('view');
		$('#grey').toggleClass('view');
	});	

	$('.circle').on('click', function(e){
		e.preventDefault();
		
		//Get id
		$('.circle').removeClass('active');
		var id = $(this).data('id');
		$(this).addClass('active');

		$('.item-service').removeClass('fade-in-bottom');
		$('.item-service').addClass('fade-in-right');
		$('#item-'+id).addClass('fade-in-bottom');

	});

	$('.circle-one').on('click', function(){
		$('#competitive .img').css('background-image','url('+ $(this).data('img') +')');
		$('.line-select').css('transform','translate(-50%, -50%) rotate(57deg)');
	});

	$('.circle-two').on('click', function(){
		$('#competitive .img').css('background-image','url('+ $(this).data('img') +')');
		$('.line-select').css('transform','translate(-50%, -50%) rotate(123deg)');
	});

	$('.circle-three').on('click', function(){
		$('#competitive .img').css('background-image','url('+ $(this).data('img') +')');
		$('.line-select').css('transform','translate(-50%, -50%) rotate(0deg)');
	});

	$('.circle-four').on('click', function(){
		$('#competitive .img').css('background-image','url('+ $(this).data('img') +')');
		$('.line-select').css('transform','translate(-50%, -50%) rotate(180deg)');
	});

	$('.circle-five').on('click', function(){
		$('#competitive .img').css('background-image','url('+ $(this).data('img') +')');
		$('.line-select').css('transform','translate(-50%, -50%) rotate(303deg)');
	});

	$('.circle-six').on('click', function(){
		$('#competitive .img').css('background-image','url('+ $(this).data('img') +')');
		$('.line-select').css('transform','translate(-50%, -50%) rotate(237deg)');
	});

	$('.circle-click').on('click', function(e){
		e.preventDefault();

		$('.circle-click').removeClass('active');
		$(this).addClass('active');

		var title = $(this).data('title');
		var info = $(this).data('text');

		$('#title-info').html(title);
		$('#list-info').html(info);
	});

	 $(window).scroll(function() {    
	    var scroll = $(window).scrollTop();
	    if (scroll >= 70) {
	        $("#navbar").addClass("fixed");
	    } else {
	        $("#navbar").removeClass("fixed");
	    }
	});

	 $('.owl-capabilities .item').on('click', function(e){
	 	e.preventDefault();

	 	$('.item').removeClass('active');
	 	$(this).toggleClass('active');

	 	var id = $(this).data('id');

	 	//Hidden info
	 	$('.slide-info').addClass('fade-in-right');

	 	//Appear info
	 	$('#'+id).removeClass('fade-in-right');
	 	$('#'+id).addClass('fade-in-left');

	 });

	 //Home
	 $('.owl-capabilities').owlCarousel({
	    loop:false,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        768: {
	        	items:2
	        },
	        1000:{
	            items:3,
	            margin:10
	        },
	        1366: {
	        	items:3,
	        	margin:30
	        },
	        1400: {
	        	items:3,
	        	margin:60
	        }
	    }
	});

	$('.owl-quotation').owlCarousel({
		loop:true,
	    nav:false,
	    responsive:{
	        0:{
	            items:1
	        },
	        600:{
	            items:1
	        },
	        1000:{
	            items:1
	        },
	        1366:{
				items:2,
				margin:18
	        },
	        1400: {
	        	items:3,
	        	margin:25
	        }
	    }
	});

}); // jQuery End
