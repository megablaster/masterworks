<?php get_header(); ?>
<?php echo do_shortcode('[rev_slider alias="services"][/rev_slider]');?>

<section id="capabilities">
	<div class="container">
		<div class="row">
			<div class="col-12">

				<h2 class="title-primary">
					<span class="blue">
						<div class="line"></div>
					</span>
					<div class="text">Our capabilities</div>
				</h2>
				
				<div class="owl-carousel owl-theme owl-capabilities">

					<div class="item active" data-id="program">
						<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/cap-1.jpg;'?>');">
							<div class="text">
								<h3>Program Management</h3>
								<a href="#" class="btn btn-more">Know More</a>
							</div>
						</div>
					</div>

					<div class="item" data-id="quality">
						<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/cap-2.jpg;'?>');">
							<div class="text">
								<h3>Quality Control</h3>
								<a href="#" class="btn btn-more">Know More</a>
							</div>
						</div>
					</div>

					<div class="item" data-id="quote">
						<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/cap-3.jpg;'?>');">
							<div class="text">
								<h3>Quote documentation requirement</h3>
								<a href="#" class="btn btn-more">Know More</a>
							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
	</div>
	<div class="row slide-info" id="program">
		<div class="col-xl-4 col-lg-4">
			<div class="img" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/bg-info.jpg'?>');"></div>
		</div>
		<div class="col-xl-6 col-lg-8 p-relative">
			<div class="info">
				<h2>Program Management <span>Ensures Successful Electronics</span> Manufacturing Projects and Relationships</h2>
				<p>For nearly 20 years we’ve seen first-hand how individual attention and responsive communication are vital to successful customer relationships. For every incoming Masterwork Electronics project, customers are assigned a dedicated Program Manager as the key contact. The Program Manager works in collaboration with a broader Masterwork team to handle purchasing, production planning, engineering and quality control.</p>
				<p>With material acquisition and management a considerable cost factor for our customers, we have refined our procurement process to significantly lower these costs and pass along the savings. Our purchasing team maximizes material savings and brings leveraged buying power through supply-chain management and strategic alliances, while our supplier certification program ensures the highest material quality.</p>
				<ul>
					<li>Supply Chain Management</li>
					<li>Customer-Focused Team </li>
					<li>Financial Strength </li>
					<li>Market Research + Planning</li>
					<li>Solutions Providers</li>
					<li>Kan Ban Managed Inventory</li>
				</ul>
			</div>
		</div>
	</div>
	<div class="row slide-info" id="quality">
		<div class="col-xl-4 col-lg-4">
			<div class="img" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/bg-info-2.jpg'?>');"></div>
		</div>
		<div class="col-xl-6 col-lg-8">
			<div class="info">
				<h2>Unprecedented <span>EMS Quality Control Standards</span></h2>
				<p>Every employee of Masterwork Electronics is committed to meeting the highest standards of the Electronics Manufacturing Services industry.</p>
				<p>Our years of experience in PCBA and cable and harness assembly have enabled us to refine our manufacturing process and fine-tune our capabilities into a system of best practices.</p>
				<p>By using only the most advanced machinery and equipment and highly trained, experienced workers, we are ISO 9001: 2008 certified, and have developed a process that meets the most rigorous quality control standards in the industry.</p>
				<p>We stand behind our work and work one-on-one with clients to make sure we meet every expectation.</p>
			</div>
		</div>
	</div>
	<div class="row slide-info" id="quote">
		<div class="col-xl-4 col-lg-4">
			<div class="img" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/bg-info-3.jpg'?>');"></div>
		</div>
		<div class="col-xl-7 col-lg-8">
			<div class="info">
				<h2>Our Detailed Quote Documentation Process allows <span>for Specialization and Adaptability</span> in Manufacturing</h2>
				<p>We use specific quote documentation requirements for our different Electronics Manufacturing Services to ensure the efficiency and accuracy of each order.</p>
				<p>Requirements vary depending on the service being provided.</p>
				<p>See below for each EMS Quote Documentation Requirement for consignment assembly, turnkey assembly, testing and measurement, and box build/distribution.</p>

				<div class="owl-carousel owl-theme owl-quotation">
					
					<div class="item">
						<h3>FOR QUOTATION</h3>
						<ul>
							<li>Bill of Materials in Excel or equivalent format with internal part numbers</li>
							<li>Approved Manufacturer List (A.M.L.) - approved manufacturer name, part number, part description</li>
							<li>Approved vendor list (preferred, not required) Assembly Drawings</li>
							<li>Assembly Instructions</li>
							<li>Sample board (preferred, not required)</li>
						</ul>
					</div>
					<div class="item">
						<h3>FOR CONSIGNMENT ASSEMBLY</h3>
						<ul>
							<li>All of the above plus:</li>
							<li>Bare circuit board and/or Gerber files</li>
							<li>Current revision sample board</li>
							<li>X, Y or centroid data files (SMT only)</li>
							<li>No load list if applicable</li>
						</ul>
					</div>
					<div class="item">
						<h3>FOR TESTING & MEASUREMENT</h3>
						<ul>
							<li>Schematic drawing</li>
							<li>Testing specifications</li>
							<li>Net list file (electronic copy preferred)</li>
							<li>Gold board (tested, functional, and current revision) </li>
						</ul>
					</div>
					<div class="item">
						<h3>FOR BOX BUILD/DISTRIBUTION</h3>
						<ul>
							<li>All of the above plus assembly instructions for Box Build</li>
							<li>Approved vendor list (AVL) for packaging</li>
							<li>Artwork(s) for packaging</li>
							<li>Customer destination shipment list</li>
							<li>Special shipping instructions (as required)</li>
						</ul>
					</div>
					<div class="item">
						<h3>FOR TURNKEY ASSEMBLY</h3>
						<ul>
							<li>All of the above plus:</li>
							<li>Gerber or CAD data file (electronic copy)</li>
							<li>PCB fabrication drawing</li>
							<li>Programming information for programmable devices</li>
						</ul>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>

<section id="services">
	
	<div class="row item-service" id="item-1">
		<div class="col-xl-5 offset-xl-2 col-lg-6 offset-lg-1">
			<div class="info">
				<h2 class="title-primary">
					<span class="blue">
						<div class="line"></div>
					</span>
					<div class="text">Services</div>
				</h2>
				<h2>Extensive Range of <span>Electronics Manufacturing </span><span class="final">Services</span> (EMS).</h2>
				<h3>Customized Printed Circuit Board Assembly (PCBA)</h3>
				<p>Masterwork Electronics provides printed circuit board assembly services to a variety of different industries. We have the capacity to handle jobs of any size and complexity and our efficient manufacturing process and nearshore location supports rapid turnaround times.</p>
				<p>PCBA services include:</p>
				<ul>
					<li>Consignment assembly</li>
					<li>Turnkey assembly</li>
					<li>Testing & measurement</li>
					<li>Custom assignments</li>
				</ul>
				<p>Our flexible, personalized service lets us work with customers on a wide range of needs, be it fabrication, or system-level assembly services.</p>
				<p>See our Quote Documentation Requirements for more details on PCBA options.</p>
				<a href="#" class="btn btn-contact">Contact</a>
			</div>
		</div>
		<div class="col-xl-4 offset-xl-1 col-lg-4 offset-lg-1">
			<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/service-1.jpg';?>')"></div>
		</div>
	</div>

	<div class="row item-service" id="item-2">
		<div class="col-xl-5 offset-xl-2 col-lg-6 offset-lg-1">
			<div class="info">
				<h2 class="title-primary">
					<span class="blue">
						<div class="line"></div>
					</span>
					<div class="text">Services</div>
				</h2>
				<h2>Extensive Range of <span>Electronics Manufacturing </span><span class="final">Services</span> (EMS).</h2>
				<h3>Box Build</h3>
				<p>We customize Box-build production lines for just-in-time order fulfillment and if preferred by the customer, build to stock and provide on-demand fulfillment directly to our customers’ end users. </p>
				<p>We have extensive warehouse operations to handle large components and inventory items as well as to carry pallets of finished goods for our customers.</p>
				<a href="#" class="btn btn-contact">Contact</a>
			</div>
		</div>
		<div class="col-xl-4 offset-xl-1 col-lg-4 offset-lg-1">
			<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/service-2.jpg';?>')"></div>
		</div>
	</div>

	<div class="row item-service" id="item-3">
		<div class="col-xl-5 offset-xl-2 col-lg-6 offset-lg-1">
			<div class="info">
				<h2 class="title-primary">
					<span class="blue">
						<div class="line"></div>
					</span>
					<div class="text">Services</div>
				</h2>
				<h2>Extensive Range of <span>Electronics Manufacturing </span><span class="final">Services</span> (EMS).</h2>
				<h3>A Leader in Cable and Harness Assembly</h3>
				<p>Masterwork Electronics is a leader and innovator in the cable and harness assembly field and welcomes one-stop turnkey wire harness and cable assembly service requests of any scale and complexity.</p>
				<p>We have the capability and expertise to work with a wide variety of cables & harnesses, including:</p>
				<ul>
					<li>Flat ribbon</li>
					<li>Coax cable assemblies</li>
					<li>Discrete harnesses</li>
					<li>Multi conductor</li>
					<li>Sensor cable/harnesses	</li>
				</ul>
				<p>Refer to our Quote Documentation Requirements page for more information</p>
				<a href="#" class="btn btn-contact">Contact</a>
			</div>
		</div>
		<div class="col-xl-4 offset-xl-1 col-lg-4 offset-lg-1">
			<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/service-3.jpg';?>')"></div>
		</div>
	</div>
	
	<div class="row item-service" id="item-4">
		<div class="col-lg-5 offset-lg-2">
			<div class="info">
				<h2>Extensive Range of <span>Electronics Manufacturing </span><span class="final">Services</span> (EMS).</h2>
				<h3>Direct Order Fulfillment and Repair Services</h3>
				<p>Our operations are tailored to provide post-production requirements for Warehousing, Pick-and-Pack and Direct Order Fulfillment for shipment to your end customers.</p>
				<p>We also handle Accessories and any warranty repair and other post sale services you may need.</p>
				<a href="#" class="btn btn-contact">Contact</a>
			</div>
		</div>
		<div class="col-lg-4 offset-lg-1">
			<div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/service-4.jpg';?>')"></div>
		</div>
	</div>
	<ul class="button">
		<li>
			<div class="circle active" data-id="1" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-1.png';?>');">
				<div class="line">
					<h3>PCBA</h3>
				</div>
			</div>
		</li>
		<li>
			<div class="circle" data-id="2" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-2.png';?>');">
				<div class="line two">
					<h3>BOX BUILD</h3>
				</div>
			</div>
		</li>
		<li>
			<div class="circle" data-id="3" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-3.png';?>');">
				<div class="line three">
					<h3>CABLE/HARNESS ASSEMBLY</h3>
				</div>
			</div>
		</li>
		<li>
			<div class="circle" data-id="4" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/home/icon-4.png';?>');">
				<div class="line four">
					<h3>Direct order fulfillment and repar services</h3>
				</div>
			</div>
		</li>
	</ul>
</section>
<section id="competitive">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="title-primary">
					<span class="blue">
						<div class="line"></div>
					</span>
					<div class="text">Mei Competitive value proposition</div>
				</h2>
				<h3>Full Lifecycle <span>"End-to-End" Solutions</span></h3>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-10">
			<div class="row">
				<div class="col-xl-7 col-lg-7 col-md-12">
					<div class="img" style="background-image:url('<?php echo get_stylesheet_directory_uri().'/img/home/competitive/img-1.jpg';?>');"></div>
				</div>
				<div class="col-xl-5 col-lg-5 col-md-12 text-center">
					<div class="circle-big">

						<div class="circle-click circle-one active"
						title="Design & Engineering"
						data-title="Design & Engineering"
						data-text="<li>- Design Services turnkey product development</li><li>- Concept & Feasibility</li><li>- Risk Analysis</li><li>- Mechanical, Electrical, Fluidics, Optical, Software, Test</li><li>- Regulatory Guidance & Compliance</li>"
						data-img="<?php echo get_stylesheet_directory_uri().'/img/home/competitive/img-1.jpg';?>"></div>

						<div class="circle-click circle-two "
						title="Rapid prototyping & Npi"
						data-title="Rapid prototyping & Npi"
						data-text="<li>- Express Rapid Protoyping Centers</li><li>- DFX & Value Analysis</li><li>- Pilot and Low-Value Runs</li><li>- Test Design & Development</li><li>- Manufacturing Transfer & NPI Gateways</li>"
						data-img="<?php echo get_stylesheet_directory_uri().'/img/home/competitive/img-2.jpg';?>"></div>

						<div class="circle-click circle-three "
						title="Regional & Global Manufacturing"
						data-title="Regional & Global Manufacturing"
						data-text="<li>- Global Manufacturing (PCBA; Cable/Harness; Systems Integration)</li><li>- Low-to-Med Volume</li><li>- Medium-to-High Mix</li><li>- Complete Test Design & Development</li><li>- Ultra-Fine Pitch, BGA, RF Testing, EMI Shielding</li><li>- Automated Conformal Coating, Potting</li>"
						data-img="<?php echo get_stylesheet_directory_uri().'/img/home/competitive/img-3.jpg';?>"></div>

						<div class="circle-click circle-four "
						title="Integration & system test"
						data-title="Integration & system test"
						data-text="<li>- Lean Inventory & Supply Chain Programs</li><li>- Complex RF & Large Electro-Mechanical</li><li>- Servers, Backplanes, Custom Cabling</li><li>- Device Programming</li><li>- Build & Configure to Order</li>"
						data-img="<?php echo get_stylesheet_directory_uri().'/img/home/competitive/img-4.jpg';?>"></div>

						<div class="circle-click circle-five "
						title="Fulfillment & logistics"
						data-title="Fulfillment & logistics"
						data-text="<li>- Serialization & Tracking</li><li>- Direct Order Fulfillment</li><li>- Flexible Financial Models</li><li>- Electronic Data Exchange & Portal</li><li>- Tailored Product Supply</li><li>- Routing & Distribution</li>"
						data-img="<?php echo get_stylesheet_directory_uri().'/img/home/competitive/img-5.jpg';?>"></div>

						<div class="circle-click circle-six "
						title="After-market services"
						data-title="After-market services"
						data-text="<li>- Repair, Warranty & Refurbishment</li><li>- Advanced Exchange</li><li>- Sustaining Engineering</li><li>- Reverse Losgistics</li><li>- Failure Analysis & Upgrades</li><li>- EOL SUpport: Take-Back, Tear-Down, Recycling</li>"
						data-img="<?php echo get_stylesheet_directory_uri().'/img/home/competitive/img-6.jpg';?>"></div>

						<div class="line"></div>
						<div class="inner-circle">
							<div class="line-select">
								<div class="point"></div>
							</div>
							<div class="text">
								<h4 id="title-info">Design & Engineering</h4>
								<ul id="list-info">
									<li>- Design Services turnkey product development</li>
									<li>- Concept & Feasibility</li>
									<li>- Risk Analysis</li>
									<li>- Mechanical, Electrical, Fluidics, Optical, Software, Test</li>
									<li>- Regulatory Guidance & Compliance</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section id="contact">
	<div class="container">
		<div class="row">
			<div class="col-lg-12">
				<h2 class="title-primary">
					<span class="blue">
						<div class="line"></div>
					</span>
					<div class="text">Contact</div>
				</h2>
				<h3><span>Get in</span> Touch <span>with Us</span></h3>

				<div class="row">

					<div class="col-lg-8 offset-lg-2 text-center">

                        <?php echo do_shortcode('[contact-form-7 id="76" title="Contact bottom"]');?>

					</div>
				</div>

			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>