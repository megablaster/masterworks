<?php get_header(); ?>

<?php while(have_posts()): the_post()?>
<?php endwhile?>

<section id="sectors">
	<section id="header" style="background-image:url('<?php the_post_thumbnail_url();?>');">
		<div class="text">
			<h1><?php the_field('title');?></h1>
		</div>
	</section>
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="info">
					<h2 class="title-primary">
						<span class="blue">
							<div class="line"></div>
						</span>
						<div class="text"><?php the_field('subtitle'); ?></div>
					</h2>
					<?php the_content();?>
				</div>
			</div>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<?php for($i=1;$i < 21; $i++): ?>
				<div class="col-xl-3">
					<img src="<?php echo get_stylesheet_directory_uri().'/img/sector/icon-';?><?php echo $i;?>.jpg" class="img-fluid">
				</div>
			<?php endfor ?>
		</div>
	</div>
	<div class="container-fluid p-none">
		<div class="row">
			<div class="col-lg-12">

				<?php
					$args = array(
						'post_type' => 'sectors',
						'posts_per_page' => -1
					);

					$q = new WP_Query($args);
				?>

				<div class="owl-carousel owl-industrial">
					<?php while($q->have_posts()): $q->the_post() ?>

						<div class="item">
							<div class="cube" style="background-image:url('<?php the_post_thumbnail_url();?>');">
								<div class="text">
									<h3><?php the_field('subtitle');?></h3>
									<a href="<?php the_permalink(); ?>">Know more</a>
								</div>
							</div>
						</div>

					<?php endwhile ?>
				</div>

			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>