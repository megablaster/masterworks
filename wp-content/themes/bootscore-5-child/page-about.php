<?php get_header(); ?>

<style>
    #navbar {
        top:0!important;
        position: fixed!important;
        width: 100%!important;
        z-index: 9999!important;
        margin-top: 0;
        background-color: #056277;
    }

    .bg-black {
        border-radius:0!important;
    }
</style>
<ul id="menu-about">
    <a data-menuanchor="oneSection" href="#oneSection">
        <span>INTRO</span>
    </a>
    <a data-menuanchor="twoSection" href="#twoSection">
        <span>FOUNDED</span>
    </a>
    <a data-menuanchor="threeSection" href="#threeSection">
        <span>WHAT DRIVES OUR SUCCESS?</span>
    </a>
    <a href="#fourSection" data-menuanchor="fourSection">
        <span>MEET OUR TEAM LEADERS</span>
    </a>
    <a data-menuanchor="fiveSection" href="#fiveSection">
        <span>QUICK FACTS</span>
    </a>
    <a data-menuanchor="sixSection" href="#sixSection">
        <span>CONTACT</span>
    </a>
</ul>
<section id="fullpage">
    <section class="vertical section-1">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-7 offset-lg-1 height">
                    <div class="text">
                        <h1>a Leader in electronic manufacturing services</h1>
                        <p>Every day, Masterwork Electronics produces more than 10,000 printed circuit board assemblies (PCBA) for leading global companies. Our boards are the "brains" behind some of the world's most innovative products, like battery chargers for electric vehicles, the latest Coke and Pepsi "smart" machines and circuit boards for cutting-edge M2M devices.</p>
                        <a href="#" class="btn btn-green">Contact</a>
                    </div>
                </div>
                <div class="col-xl-6 offset-xl-1 col-lg-3 offset-lg-1 height p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/section-1.jpg';?>')
                            "></div>
                </div>
            </div>
        </div>

        <ul class="float-button">
            <li>
                <a href="<?php echo home_url('about');?>" class="active">
                    <span>About</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('why-nearshoring');?>">
                    <span>Why nearshoring?</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('production-facilities');?>">
                    <span>Production facilities</span>
                </a>
            </li>
            <li>
                <a href="<?php echo home_url('equipment-list');?>">
                    <span>Equipment list</span>
                </a>
            </li>
        </ul>

    </section>
    <section class="vertical section-2">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-4 offset-xl-1 col-lg-7 offset-lg-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Founded</div>
                        </h2>
                        <h3>Founded by entrepreneurs in 1994 to meet the increasing worldwide demand for PCBA and Cable & Harness Assembly</h3>
                        <strong>Masterwork Electronics is today one of the premier Electronics Manufacturing Service (EMS)
                            providers.</strong>
                        <p>We serve a diverse mix of Original Equipment Manufacturers (OEMs) from North America and beyond, representing such varied markets as telecom, computer hardware, vending, POS devices, and pool equipment.</p>
                        <p>With California-based corporate leadership and state-of-the-art production facilities just over the border in Mexicali, Mexico, Masterwork Electronics is now the go-to nearshoring provider of high quality/low-cost manufacturing solutions.</p>
                    </div>
                </div>
                <div class="col-xl-5 offset-xl-2 col-lg-3 offset-lg-1 height p-relative">
                    <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri().'/img/about/section-2.jpg';?>')
                            "></div>
                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-3">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">The masterwork advantage</div>
                        </h2>
                        <h2 class="title">What drives <span>our success</span></h2>
                        <p>A business philosophy that puts our customers’ success at the heart of everything we do. To ensure they stay competitive, we stay competitive. Strategic alliances with supply chain partners ensure our deliverables are on-time and of the highest quality. We have flexible, personalized customer agreements, and significant investment in manufacturing equipment and facilities in nearby Mexico.</p>
                        <p>That’s what lets Masterwork Electronics offer competitive electronics manufacturing services that:</p>
                        <div class="row">
                            <div class="col col-md-1"></div>
                            <div class="col col-md-3">
                                <div class="info">
                                    <div class="img" style="background-image: url('<?php echo
                                        get_stylesheet_directory_uri().'/img/home/near-2.jpg';?>')"></div>
                                    <h3>Shorten product development cycles</h3>
                                </div>
                            </div>
                            <div class="col col-md-3">
                                <div class="info">
                                    <div class="img" style="background-image: url('<?php echo
                                        get_stylesheet_directory_uri().'/img/home/near-3.jpg';?>')"></div>
                                    <h3>Reduce total cost of ownership</h3>
                                </div>
                            </div>
                            <div class="col col-md-3">
                                <div class="info">
                                    <div class="img" style="background-image: url('<?php echo
                                        get_stylesheet_directory_uri().'/img/home/near-4.jpg';?>')"></div>
                                    <h3>Increase asset utilization</h3>
                                </div>
                            </div>
                            <div class="col col-md-1"></div>
                            <div class="col-xl-12">
                                <p>As Masterwork Electronics continues to grow, we stay focused on driving up efficiencies and driving out cost for the benefit of our customers, suppliers, and employees.</p>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-4">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-8 offset-xl-2 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Our team</div>
                        </h2>
                        <h2 class="title">Get to know the leaders of <span>our team</span></h2>
                        <div class="row">
                            <?php
                                $args = array(
                                    'post_type' => 'ourteam',
                                    'posts_per_page' => -1
                                );

                                $q = new WP_Query($args);
                            ?>

                            <?php while($q->have_posts()): $q->the_post() ?>
                                <div class="col-xl-3 col-md-3">
                                    <div class="circle" style="background-image: url('<?php the_post_thumbnail_url();
                                    ?>')">
                                        <a href="#<?php echo the_ID();?>" class="text fancybox">Know more</a>
                                    </div>
                                    <h3><?php the_title();?></h3>
                                </div>
                            
                            <div id="<?php echo the_ID();?>" style="display: none;max-width: 700px;width:100%;
                            background-color:#606060;">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-xl-12">
                                            <h2 style="color:white;"><?php the_title();?></h2>

                                            <?php if(have_rows('videos')): ?>

                                                <?php while(have_rows('videos')): the_row() ?>
                                                    <a class="fancybox" href="<?php the_sub_field('url_video');?>"
                                                       style="display:block;font-weight:300;font-size:14px;
                                                       color:white;">
                                                        <?php the_sub_field('title');?>
                                                    </a>
                                                <?php endwhile ?>

                                            <?php endif ?>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <?php endwhile ?>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php wp_reset_postdata();?>
    </section>
    <section class="vertical section-5">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-10 offset-xl-1 height">
                    <div class="top">
                        <h2 class="title-primary">
                           <span class="blue">
                              <div class="line"></div>
                           </span>
                            <div class="text">Quick facts</div>
                        </h2>
                        <h2 class="title">Get to know some facts about <span>Masterwork Electronics</span></h2>
                        <div class="row">
                            <div class="col-xl-2 col-sm-4">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-1.png';?>')">
                                </div>
                                <h3>HQ: San Diego Revenue: ~ $100M / 550+ H/C</h3>
                            </div>
                            <div class="col-xl-2 col-sm-4">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-2.png';?>')">
                                </div>
                                <h3>Founded in 1994. 26 years of successful operations, 16 years in Mexicali</h3>
                            </div>
                            <div class="col-xl-2 col-sm-4">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-3.png';?>')">
                                </div>
                                <h3>Acquired in June 2018 by Hidden Harbor Capital Partners (hh-cp.com)</h3>
                            </div>
                            <div class="col-xl-2 col-sm-4">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-4.png';?>')">
                                </div>
                                <h3><span>Sector Focus:</span>
                                    <ul>
                                        <li>Industrial · High-End Communications</li>
                                        <li>IOT · Power/Energy</li>
                                        <li>Transportation/Automotive</li>
                                        <li>Medical · A&D</li>
                                    </ul>
                                </h3>
                            </div>
                            <div class="col-xl-2 col-sm-4">
                                <div class="img" style="background-image: url('<?php echo get_stylesheet_directory_uri
                                    ().'/img/about/icon-5.png';?>')">
                                </div>
                                <h3>Mexicali Operations
                                    original PCBA Site opened in 2004.
                                    Two more Sites opened in 2018 (Box)
                                    and 2019 (Fulfillment) ~ 90K+ SF Capacity</h3>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <section class="vertical section-6">
        <div class="top" id="contact">
            <div class="row">
                <div class="col-xl-8 offset-xl-1">
                    <h2 class="title-primary">
                            <span class="blue">
                                <div class="line"></div>
                            </span>
                        <div class="text">Contact</div>
                    </h2>
                    <h3><span>Get in</span> Touch <span>with Us</span></h3>
                </div>
                <div class="col-lg-6 offset-lg-3 text-center">

                    <?php echo do_shortcode('[contact-form-7 id="76" title="Contact bottom"]');?>

                </div>
            </div>
        </div>
    </section>
</section>

<?php get_footer(); ?>
