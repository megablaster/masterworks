<?php
	/**
	 * The header for our theme
	 *
	 * This is the template that displays all of the <head> section and everything up until <div id="content">
	 *
	 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
	 *
	 * @package Bootscore
	 */
	
	?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <!-- Favicons -->
    <link rel="apple-touch-icon" sizes="180x180" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/site.webmanifest">
    <link rel="mask-icon" href="<?php echo get_stylesheet_directory_uri();?>/img/favicon/safari-pinned-tab.svg" color="#0d6efd">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">
    <?php wp_enqueue_script('jquery'); ?>
    <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

    <div id="to-top"></div>

    <div id="page" class="site">

        <div id="navbar">
            <div class="container">
                <div class="row">
                    <div class="col-xl-4 col-lg-4 col-md-4 col-6 p-relative">
                        <a href="<?php echo home_url('/');?>">
                            <img src="<?php echo get_stylesheet_directory_uri().'/img/logo/logo-green.png';?>" alt="<?php bloginfo('name');?>" class="img-fluid logo">
                        </a>
                    </div>
                    <div class="col-xl-8 col-lg-8 col-md-8 col-6">
                        <div class="menu-mobile">
                            <div class="top-center">
                                <div class="bg-green">
                                    <ul>
                                        <li><a href="<?php echo home_url('/');?>">Home</a></li>
                                        <li><a href="<?php echo home_url('about');?>">About</a></li>
                                        <li class="has-submenu">
                                            <a href="#" class="primary">Solutions</a>
                                            <div class="submenu">
                                                <div class="row">
                                                    <div class="col-xl-6">
                                                        <h4>Sectors</h4>
                                                        <?php
                                                            $args = array(
                                                                'post_type' => 'sectors',
                                                                'posts_per_page' => -1
                                                            );

                                                            $q = new WP_Query($args);
                                                        ?>
                                                        <ul>
                                                            <?php while($q->have_posts()): $q->the_post() ?>
                                                                <li><a href="<?php the_permalink();?>"><?php
                                                                        the_title();?></a></li>
                                                            <?php endwhile ?>
                                                        </ul>
                                                        <?php wp_reset_postdata(); ?>
                                                    </div>
                                                    <div class="col-xl-6">
                                                        <h4>Services</h4>
                                                        <ul>
                                                            <li><a href="#">PCBA</a></li>
                                                            <li><a href="#">Box Build</a></li>
                                                            <li><a href="#">Cable/Harness Assembly</a></li>
                                                            <li><a href="#">Direct Order & Repair Services</a></li>
                                                        </ul>
                                                        <a href="#" class="title">End-to-End Solutions</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </li>

                                        <li><a href="<?php echo home_url('case-study');?>">Case Study</a></li>
                                        <li><a href="<?php echo home_url('career');?>">Career</a></li>
                                        <li><a href="<?php echo home_url('contact');?>">Contact</a></li>
                                        <li class="close-icon">
                                            <a href="#">
                                                <i class="far fa-times-circle fa-3x"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="bg-black p-relative">
                                    <span class="phone">
                                        <i class="fas fa-phone"></i>
                                    </span>
                                    <ul class="call">
                                        <li><a href="tel:7075889906">Call Us<span>707.588.9906</span></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <a href="#" class="burger">
                            <i class="fas fa-bars fa-3x"></i>
                        </a>
                    </div>
                    <!-- <div class="col-lg-1">
                        <?php if ( is_active_sidebar( 'lang-top' )) : ?>
                            <?php dynamic_sidebar( 'lang-top' ); ?>
                        <?php endif; ?>
                    </div> -->
                </div>
            </div>
        </div>

        <div id="grey"></div>

        <?php bootscore_ie_alert(); ?>
