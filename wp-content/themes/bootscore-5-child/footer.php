<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Bootscore
 */

?>

<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-4">
                <div class="info">
                    <h3>Solutions</h3>
                    <div class="line"></div>
                    <?php
                        $args = array(
                            'post_type' => 'sectors',
                            'posts_per_page' => -1
                        );

                        $q = new WP_Query($args);
                    ?>
                    <ul>
                        <?php while($q->have_posts()): $q->the_post() ?>
                            <li><a href="<?php the_permalink();?>"><?php
                                    the_title();?></a></li>
                        <?php endwhile ?>
                    </ul>
                    <?php wp_reset_postdata(); ?>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="info">
                    <h3>Contact us</h3>
                    <div class="line"></div>
                    <ul>
                        <li><a href="#">info@example.com</a></li>
                        <li><a href="#">www.example.com</a></li>
                        <li><a href="#">+01234567890</a></li>
                        <li><a href="#">+01234567890</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-3">
                <img src="<?php echo get_stylesheet_directory_uri().'/img/logo/logo-footer.png';?>" class="img-fluid logo">
            </div>
            <div class="col-lg-2">
                <p class="copy">&copy; Copyrighted. Masterwork Electronics.</p>
            </div>
        </div>
    </div>
</footer>

<div class="top-button">
    <a href="#to-top" class="btn btn-primary shadow"><i class="fas fa-chevron-up"></i></a>
</div>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
